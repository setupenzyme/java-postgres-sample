package com.cloudsenz.springbootsample.controller;

import java.util.List;
import java.util.Optional;

import com.cloudsenz.springbootsample.model.Todo;
import com.cloudsenz.springbootsample.repository.TodoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class TodoController {
    @Autowired
    private TodoRepo todoRepo;

    @GetMapping(path = "/")
    public String showTodos(Model model) {
        List<Todo> todos = (List<Todo>) todoRepo.findAll();
        model.addAttribute("todos", todos);
        model.addAttribute("todo", new Todo());
        return "index";
    }

    @PostMapping(path = "/")
    public String createTodo(@ModelAttribute Todo todo) {
        todoRepo.save(todo);
        return "redirect:/";
    }

    @PostMapping(path = "/{id}")
    public String editTodo(@PathVariable Integer id, @ModelAttribute Todo todoObj) {
        Optional<Todo> todo = todoRepo.findById(id);

        if(todo.isPresent()) {
            todo.get().setTask(todoObj.getTask());
            todoRepo.save(todo.get());

            return "redirect:/";
        }
        return "404";
    }

    @GetMapping(path = "/update/{id}")
    public String getEditPage(Model model, @PathVariable Integer id) {
        Optional<Todo> todo = todoRepo.findById(id);

        if(todo.isPresent()) {
            model.addAttribute("todo", todo.get());
            model.addAttribute("todoObj", new Todo());
            return "update";
        }
        return "404";
    }

    @GetMapping(path="/delete/{id}")
    public String deleteTodo(@PathVariable Integer id) {
        todoRepo.deleteById(id);
        return "redirect:/";
    }
}
