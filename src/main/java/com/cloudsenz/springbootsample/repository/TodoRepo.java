package com.cloudsenz.springbootsample.repository;

import com.cloudsenz.springbootsample.model.Todo;
import org.springframework.data.repository.CrudRepository;

public interface TodoRepo extends CrudRepository<Todo, Integer> { }
