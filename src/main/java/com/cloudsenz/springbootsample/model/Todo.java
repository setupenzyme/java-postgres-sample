package com.cloudsenz.springbootsample.model;

import javax.persistence.*;

@Entity
@Table(name="todos")
public class Todo {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(name = "task", nullable = false)
    String task;

     public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }
}
