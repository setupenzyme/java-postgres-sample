-- liquibase formatted sql
-- changeset liquibase:1

CREATE TABLE IF NOT EXISTS todos(
    id SERIAL PRIMARY KEY,
    task VARCHAR(255)
);

--rollback DROP TABLE todos;